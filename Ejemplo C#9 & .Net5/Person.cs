﻿using System;
namespace Sample_DotNet5_CSharp9
{
    public record Person
    {
        public int Id { get; init; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
    }

    public record Worker : Person
    {
        public string Job { get; set; }
        public double Salary { get; set; }
    }

    public record Student : Person
    {
        public School School { get; set; }
        public string[] Courses { get; set; }
    }

    public record Professor : Worker
    {
        public School School { get; set; }
        public string Course { get; set; }
    }

    public record School
    {
        public string Name { get; set; }
    }
}
