﻿using System;

namespace Sample_DotNet5_CSharp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating school...");
            var school = new School
            {
                Name = "Fidelitas"
            };
            Console.WriteLine(school.ToString());
            Console.ReadLine();

            Console.WriteLine("Creating 2 students...");
            var student1 = new Student
            {
                Id = 1,
                Name = "Maria",
                LastName = "Smith",
                Age = 22,
                Address = "San Jose, CR",
                Courses = new String[] {
                    "Programacion Avanzada",
                    "Intro Inteligencia Artificial"
                },
                School = school
            };

            var student2 = new Student
            {
                Id = 2,
                Name = "Pedro",
                LastName = "Smith",
                Age = 26,
                Address = "Cartago, CR",
                Courses = new String[] {
                    "Programacion Avanzada",
                    "Intro Inteligencia Artificial"
                },
                School = school
            };

            Console.WriteLine(student1.ToString());
            Console.WriteLine(student2.ToString());
            Console.ReadLine();

            Console.WriteLine("Creating a professor...");
            var professor = new Professor
            {
                Id = 1,
                Name = "Luis",
                LastName = "Chavarria",
                Age = 42,
                Salary = 2500,
                Address = "Heredia, CR",
                Course = "Intro Inteligencia Artificial",
                Job = "Profesor Ingenieria",
                School = school
            };
            Console.WriteLine(professor.ToString());
        }
    }
}
